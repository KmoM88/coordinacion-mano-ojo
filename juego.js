let canvas = document.querySelector('canvas');
canvas.width = window.innerWidth * 0.7;
canvas.height = window.innerHeight * 0.7;

let c = canvas.getContext('2d');
let canvasBound = canvas.getBoundingClientRect();

let mouse = {
    x: undefined,
    y: undefined
}
let puntaje = 0;
var puntajeDOM = document.getElementById('puntaje');
var maximoDOM = document.getElementById('maximo');
let animationInterval = 100;
let minRadius = 50;
var colorArray = [
    '#FF0000',
    '#E83E1B',
    '#FFAC16',
    '#E8E508',
    '#32FF4C',
];
let sound1 = document.getElementById("beep1");
let sound2 = document.getElementById("beep2");

class Circle {
    constructor(minRadius) {
        let canvasBound = canvas.getBoundingClientRect();
        let timer;
        this.radius = minRadius;
        this.x = Math.random() * (canvasBound.width - this.radius * 2) + this.radius;
        this.y = Math.random() * (canvasBound.height - this.radius * 2) + this.radius;
        this.color = colorArray[Math.floor(animationInterval/20.0001)];

        this.draw = () => {
            c.clearRect(0, 0, innerWidth, innerHeight);
            c.beginPath();
            c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
            c.strokeStyle = "black";
            c.fillStyle = this.color;
            c.fill();
            c.stroke();
        };

        this.update = () => {
            c.clearRect(0, 0, innerWidth, innerHeight);
            if (this.radius > 0){
                this.radius -= 1;
                this.animation();
            }
            else {
                sound1.play();
                this.clear();
                puntajeDOM.innerHTML = puntaje.toFixed(0);
                maximoDOM.innerHTML = puntaje.toFixed(0);
                alert(`Puntaje: ${puntaje.toFixed(0)}`);
            }
            this.draw();
        }

        this.animation = () => {
            this.timer = setTimeout(circle.update, animationInterval);
        }

        this.clear = () => {
            c.clearRect(0, 0, innerWidth, innerHeight);
            clearTimeout(this.timer);
        }
    }
}

let circle = new Circle(0, 0, 0, '');

canvas.addEventListener('click', (event) => {
    mouse.x = event.x - canvasBound.left;
    mouse.y = event.y - canvasBound.top;
    if (Math.abs(mouse.x - circle.x) < circle.radius && Math.abs(mouse.y - circle.y) < circle.radius){
        sound2.play();
        puntaje = puntaje + circle.radius - Math.max(Math.abs(mouse.x - circle.x), Math.abs(mouse.y - circle.y));
        puntajeDOM.innerHTML = puntaje.toFixed(0);
        if(animationInterval >= 5)
            animationInterval-=2;
        circle.clear();
        randomCircle();
    }
    else{
        sound1.play();
        circle.clear();
        puntajeDOM.innerHTML = puntaje.toFixed(0);
        maximoDOM.innerHTML = puntaje.toFixed(0);
        alert(`Puntaje: ${puntaje.toFixed(0)}`);
    }
})

window.addEventListener('resize', () => {
    canvas.width = window.innerWidth * 0.75;
    canvas.height = window.innerHeight * 0.75;
})

function randomCircle() {
    c.clearRect(0, 0, innerWidth, innerHeight);
    circle = new Circle(minRadius)
    circle.draw();
    circle.animation();
}

function init() {
    sound2.play();
    puntaje = 0;
    animationInterval = 100;
    puntajeDOM.innerHTML = 0;
    randomCircle();
}
